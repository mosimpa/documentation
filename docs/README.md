# Monitoreo Simplificado de Pacientes en situación de internación masiva - MoSimPa

Este repositorio contiene la documentación del proyecto necesaria para poder
homologar el mismo.

El [mapa del sitio](sitemap.html) provee el mismo listado que aquí abajo pero de forma mas detallada.

- [ConfigurationManagement](ConfigurationManagement/README.md).
- [ProductIntegration](ProductIntegration/README.md).
- [TechnicalSolution](TechnicalSolution/README.md).
- [ProductDocumentation](ProductDocumentation/README.md).
- [ProjectPlanning](ProjectPlanning/README.md).
- [RequirementDevelopment](RequirementDevelopment/README.md).
- [RiskManagement](RiskManagement/README.md).
- [VerificationAndValidation](VerificationAndValidation/README.md).
