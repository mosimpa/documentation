[TOC]

# Ideas para testing

# Tests ??


## Tests de ABM

### Verificar que ABM no permita el ingreso de una nueva internación con pacientes que se encuentran asociados a otra internación activa.

Definir...

## Tests de datakeeper

### Testear que funcione al upgrade de la base de datos.

- **Objetivo:** verificar que una nueva versión de datakeeper que contenga un update a la base de datos pueda llevarlo a cabo sin problemas.
- **Pre condición:** sistema funcional, base de datos cargadas con múltiples datos y casos, sistema con versión de datakeeper anterior.
- **Procedimiento:** Proceder a la actualización de datakeeper, la base de datos deberá ser actualizada sin generar problemas.


# Tests de integración

## Tests del dispositivo

### Uso del server NTP

- **Objetivo:** verificar que el dispositivo solo funcione ando está proveído de un servicio NTP en el mismo servidor que corre el broker MQTT.
- **Pre condición:** sistema funcional.
- **Procedimiento:**

Conectarse a la consola serie del dispositivo.

En el servidor apagar chrony

```sh
sudo systemctl stop chrony.service
```

Alimentar el dispositivo. Corroborar que no comienza a transmitir datos si no obtiene tiempo por NTP.

Volver a levantar chrony:

```sh
sudo systemctl start chrony.service
```

Verificar con el log del dispositivo que puede establecer el tiempo y comienza a entregar datos.

### Caída del broker MQTT

- **Objetivo:** verificar que el dispositivo pueda seguir entregando datos ante la caída del broker MQTT y su posterior reposición en servicio.
- **Pre condición:** sistema funcional.
- **Procedimiento:**

Conectarse a la consola serie del dispositivo.

Apagar el broker MQTT:

```sh
sudo systemctl stop mosquitto.service
```

Esperar 20" de manera que el dispositivo registre la pérdida de la conexión.
Iniciar el broker MQTT:

```sh
sudo systemctl start mosquitto.service
```

Verificar con el log del dispositivo que puede restablecer la conexión.

### Caída de red WiFi

- **Objetivo:** verificar que el dispositivo pueda seguir entregando datos ante la caída de la red WiFi y su posterior reposición en servicio.
- **Pre condición:** sistema funcional.
- **Procedimiento:**

Conectarse a la consola serie del dispositivo.

Apagar el acces point que brinda el servicio.

Esperar 20" de manera que el dispositivo registre la pérdida de la conexión.

Encender el access point y esperar a que llegue a estado de servicio.

Verificar con el log del dispositivo que puede restablecer la conexión.

### Reintento de conexión a red WiFi / configuración de red WiFi errónea

- **Objetivo:** determinar que el dispositivo va a seguir intentando conectarse a la red WiFi. Notar que el orden de los primeros pasos lo diferencia del test "Caída de red WiFi".
- **Pre condición:** sistema funcional, dispositivo configurado con red WiFi apagada (inexistente).
- **Procedimiento:**

Apagar el acces point que brinda el servicio.

Conectarse a la consola serie del dispositivo.

Alimentar el dispositivo.

Verificar con el log del dispositivo que reintenta la conexión.

Encender el access point y esperar a que llegue a estado de servicio.

Verificar con el log del dispositivo que puede establecer la conexión.


## Test del sistema

### Verificar que el sistema avise al usuario cuando un valor de SpO2 o ritmo cardíaco es incorrecto

- **Objetivo:** determinar que el sistema notifica al operario en caso de que el valor de SpO2 y/o ritmo cardíaco sea potencialmente incorrecto.
- **Pre condición:** sistema funcional, dispositivo cargado en una internación.
- **Procedimiento:**

Dejar el sensor de oximetría al aire. El sistema debe mostrar que el valor no es confiable.

Apoyar el dedo y esperar como máximo 20 segundos para obtener valores medidos. El sistema debe dejar de mostrar el aviso de valor no confiable.

Volver a sacar el dedo, esperar al menos 20 segundos, el sistema deberá mostrar otra vez que el valor no es confiable.

### Verificar que los valores no son actuales luego de 30"

- **Objetivo:** determinar que el sistema notifica al operario en caso de que el valor de SpO2 y/o ritmo cardíaco haya ocurrido 30 o mas segundos antes.
- **Pre condición:** sistema funcional, dispositivo cargado en una internación.
- **Procedimiento:**

Poner en marcha el dispositivo y colocar el dedo en el sensor de oximetría.

Esperar como mucho 20" para obtener al menos un valor medido correctamente.

Desconectar la alimentación del dispositivo.

Esperar 30". El sistema deberá mostrar que los últimos valores obtenidos no son confiables.