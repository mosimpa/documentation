[TOC]

# Sección 1: introducción

<!--En esta sección deberá documentar de manera resumida el contenido
del documento, y se deberá describir el propósito del mismo.-->

Este Documento constituye en sí mismo un Plan de Gestión de la
Configuración del Proyecto MoSimPa, que incluye al software
que forma parte del mismo. A partir de este Plan se busca organizar y
facilitar la gestión de los activos/ítems de configuración generados en
cada etapa del Proyecto, y alcanza a todos aquellos que agreguen valor
al mismo, como así también a aquellos que constituyan evidencia de su
desarrollo.

A lo largo de este Documento se procederá a describir:

-   los recursos de software que serán empleados en el diseño y
    desarrollo del Proyecto en cada entorno de trabajo,
-   la identificación y tipos de ítems de configuración (IC) que serán
    puestos bajo control de cambios durante el transcurso del proyecto,
-   el estándar de nomenclatura empleado,
-   la estructura de directorios definida,
-   las líneas de base de configuración establecidas,
-   las políticas de *backup* y *restore*,
-   los mecanismos para la gestión de los cambios suscitados,
-   las auditorías de configuración que serán ejecutadas, así como
-   los reportes que serán elaborados.

## 1.1. Organización del Documento

<!--En este punto deberá describir cada una de las secciones que
conforman el documento, con el objetivo de actuar como guía del lector,
sugiriendo una secuencia de lectura.-->

A continuación se describirá brevemente cada una de las secciones que
componen el presente Documento. Se sugiere al lector una lectura
secuencial, siguiendo el orden propuesto.

**Sección 1**: – Introduce al lector al contenido del Plan.

**Sección 2** :– Describe los objetivos del Plan.

**Sección 3**: – Identifica los documentos relacionados con el Plan.

**Sección 4**: – Identifica los lectores a los cuales está dirigido el
Plan.

**Sección 5**: – Describe cómo está organizado el equipo de CM
(*Configuration Management*) y cómo se relaciona con los demás
integrantes del Proyecto.

**Sección 6**: – Identifica los recursos necesarios para la
implementación del Plan.

**Sección 7**: – Describe los ítems de configuración del Proyecto que
serán puestos bajo control de configuración, la nomenclatura utilizada
para identificarlos, la estructura de directorios que los contiene y el
método para su selección.

**Sección 8** :– Describe el proceso para implementar el circuito de
control de cambios de todas las solicitudes de cambio que arriban.

**Sección 9**:– Expone el glosario de términos del Plan y la historia de
los cambios que se realizan al Documento.
