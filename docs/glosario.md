[TOC]

# Glosario de términos y definiciones

## Siglas utilizadas

### BPF

Buenas Prácticas de Fabricación.

### CCB

(en inglés, *Change Control Board*, o Comité de Control de
Cambios).[^1 ](#footnote1)

### CU

Casos de Uso.

### CV

Ciclo de Vida.

### CVS

(en inglés*, Control Version System,* o Sistema de Control de
Versiones*).*

### D & D

Diseño y Desarrollo de Software.

### EAS

Especificación de Arquitectura del Software.

### ERS

Especificación de Requerimientos del Software.

### HW

Hardware.

### IC

Ítems o Elementos de Configuración.

### IP

Informe de Problemas.

### PEMS

(en inglés, *Programmable Electromedical System,* o Sistema
Electromédico Programable).

### PESS

(en inglés, *Programmable Electronic Sub System,* o Subsistema
Electrónico Programable).

### PM

Producto Médico.

### PP

Plan de Proyecto.

### PSCP

Plan de Seguimiento y Configuración de Proyecto.

### RSKM

(en inglés, *Risk Management,* o Gestión de Riesgos).

### SC

Solicitud de Cambio.

### SCM

(en inglés, *Software* *Configuration Management,* o Gestión de
Configuración del Software).

### SOUP

(en inglés, *Software Of Unknown Provenance,* o Software de
Procedencia Desconocida).

### SVN

Subversion.

### SW

Software.

### V & V

Verificación y Validación de Software.

### WBS

(en inglés, *Work Breakdown Structure,* o Estructura de
Desglose de Trabajo).

## Definiciones

### Backlog

Listado de la totalidad de las peticiones y tareas que se
han planificado hacer durante el desarrollo del proyecto.

### Backup

Respaldo de información.

### Bug

Error o fallo de software, que hace que el mismo no se
comporte de manera esperada.

### Board

También denominada “tablero”, permite gestionar los flujos
de trabajo o *workflows*.

### Camino feliz

También denominado “camino principal”, es el resultado
esperado en el funcionamiento del software.

### Comité de Control de Cambios

Grupo de profesionales que tienen la
responsabilidad de tomar decisiones relacionadas con la administración
de los cambios que se susciten a lo largo del proyecto.

### Control de Configuración

Proceso que permite asegurar que las
versiones de sistemas y componentes se registren y mantengan, de modo
tal que los cambios se gestionen, y se identifiquen y almacenen todas
las versiones de componentes durante la vida del sistema.

### Check-in

Véase [commit](#commit).

### Checklist

Lista de chequeo.

### Check-out

Crear una copia de trabajo local desde el repositorio.

### Commit

Confirmar los cambios realizados en la copia del trabajo
de un repositorio.

### Elicitación de requerimientos

Implica comprender las necesidades
del cliente y plasmarlas como requerimientos del sistema.

### Entregable

Resultado o salida requerida (incluye documentación) de
una actividad o tarea.

### Estrategia gratulatoria

Rama de estrategia regulatoria.

### Estructura de Desglose de Trabajo (WBS)

Herramienta utilizada para
descomponer de manera analítica un proyecto y desglosarlo en sus partes
elementales, con el objetivo de organizar el trabajo en elementos
fáciles de manejar y facilitar la comprensión del proyecto.

### Framework de aplicación

Conjunto de clases concretas y abstractas
reutilizables que implementan características comunes a muchas
aplicaciones en un dominio (por ejemplo, interfaces de usuario). Las
clases en el framework de aplicación se especializan e instancian para
crear una aplicación.

### Hito

También llamado “*milestone*”, es un punto de referencia que
marca un evento importante de un proyecto.

### Incremento

Parte del producto realizada en un período de tiempo, o
*sprint*, potencialmente entregable.

### Informe de Especificación de Arquitectura

Registro que contempla la
estructura organizativa de un sistema o componente.

### Informe de Especificación de Requerimientos del Software

Registro que contempla una descripción completa de qué servicios se requieren del
sistema, y la identificación de las restricciones sobre su operación y
desarrollo.

### Informe de Problemas

Registro que refleja el comportamiento real o
potencial de un producto software, que el usuario u otra persona
prevista considere no seguro, inapropiado para el uso previsto o
contrario a especificación.

### Informe de Testing

Registro de las pruebas realizadas al producto
software, que permite proporcionar a los *stakeholders* información
objetiva e independiente sobre la calidad del producto.

### Issue

Tarea o actividad que permite cumplir con los objetivos de
un proyecto.

### Ítem de Configuración

También llamado “elemento de configuración”,
es una entidad que puede ser identificada excepcionalmente en un punto
de referencia dado.

### Línea base de configuración

Colección de las versiones de los componentes que forman parte de un sistema, y que gracias a su control
se permite recrear en un punto del ciclo de vida el estado actual del
sistema.

### Modelo de Ciclo de Vida Incremental - Iterativo

Estructura conceptual que abarca la vida del software, desde la definición de sus
requisitos hasta su liberación, que permite contar con versiones
parciales del producto (incrementos) a medida que se va construyendo el
producto final, y donde cada versión añade funcionalidad a la versión
anterior.

### Plan de Gestión de Configuración

Documento en el cual se vuelcan
los resultados de aplicar un conjunto de actividades que permiten
gestionar los cambios que se suscitan a lo largo del ciclo de vida del
software y administrar sus diferentes versiones.

### Plan de Gestión de Riesgos

Documentación que plasma los resultados
de las tareas de análisis, evaluación y control de riesgos del producto,
tras una aplicación sistemática de las políticas de gestión,
procedimientos y prácticas.

### Plan de Proyecto

Documentación que registra el trabajo que se va a
realizar, quién lo efectuará, el calendario de desarrollo y los
productos de trabajo.

### Plan de Verificación y Validación

Documentación que plasma los
resultados de la aplicación de un conjunto de procedimientos,
actividades, técnicas y herramientas que se utilizan, paralelamente al
desarrollo, para asegurar que un producto de software cumpla con los
requerimientos especificados y con las expectativas del cliente.

### Redmine

Herramienta de gestión de proyectos.

### Release

Versión de un sistema de software que se pone a
disposición de los clientes del sistema.

### Restore

Recuperación de datos.

### Revisión

Instancias de evolución de los activos que están sometidos
a control de versiones.

### Sistema de Control de Versiones

Herramienta que permite gestionar
los cambios que se realicen sobre los ítems de configuración.

### Sistema Electromédico Programable

Equipo electromédico o sistema
electromédico que contiene uno o más subsistemas electrónicos
programables.

### Solicitud de Cambio

Especificación documentada de un cambio a
realizar a un producto software.

### SOUP

Elemento software que se ha desarrollado, y generalmente
disponible, y que no ha sido desarrollado con el propósito de ser
incorporado en el dispositivo médico, o software previamente
desarrollado para el cual los registros adecuados del proceso de
desarrollo no están disponibles.

### Sprint o Iteración

Período fijo de tiempo, donde se ejecutarán un
conjunto de actividades, repetidas ciclo a ciclo, en donde se espera
concretar la realización de módulos funcionales.

### Stakeholder

Parte interesada del proyecto.

### Startup

Encuentro introductorio entre las partes interesadas del
proyecto y los miembros del equipo consultor, que permite conocer las
características y modos de trabajo de los *stakeholders*.

### Subsistema Electrónico Programable

Sistema basado en una o más
unidades de procesamiento central, incluido su software e interfaces.

### Taggeo

Etiquetado que permite identificar fácilmente las
revisiones importantes dentro del proyecto.

### Versión

Se corresponde con momentos específicos en el tiempo en los
cuales se ha cumplido un conjunto de objetivos.

### Workflow

También denominado flujo de trabajo, es la definición
detallada de un proceso empresarial que tiene la intención de lograr
cierta tarea. Por lo general, el flujo de trabajo se expresa
gráficamente y muestra las actividades de proceso individual y la
información que produce y consume cada actividad.

<a name="footnote1"></a>**[1]** En el Informe de Incremento I denominado como CCC.
