# Manual del usuario de datakeeper

Actualizado a versión 0.0.34-1.

datakeeper es la aplicación encargada de hacer de interfase entre la base de datos y el broker MQTT. El único punto de interacción con el usuario es su archivo de configuración situado en */etc/xdg/MoSimPa/datakeeper.conf*

```
[database]
database=mosimpa-datakeeper
hostname=localhost
oldDataThresholdS=86400
password=youshouldreallychangeme
username=datakeeper

[mqtt]
broker=localhost
```

## Entradas en [database]

- **database**: el nombre de la base de datos a la cual debe conectarse datakeeper.
- **hostname**: el host al cuál debe conectarse datakeeper.
- **username**: nombre de usuario de acceso a la base de datos.
- **password**: password para el acceso a la base de datos.

En un sistema preinstalado todos los valores arriba citados estarán ya configurados.

- **oldDataThresholdS**: datakeeper eliminará de la base de datos valores de sensores mas antiguos que la cantidad de segundos determinada en este valor. Si se configura en 0 los valores no serán borrados.

## Entradas en [mqtt]

Actualmente solo se puede configurar el host del broker MQTT.
