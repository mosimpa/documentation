# MoSimPa ABM (Alta, Baja y Modificación)

![Para Ayudar](files/paraayudar.png) ![ABM](files/mosimpa-abm.png)

MoSimPa ABM (o simplemente ABM) es la aplicación utilizada por el sistema MoSimPa para la alta, baja y modificación de datos de internaciones.

![Ventana principal](files/abm_main_window.png)

El programa se divide en cuatro funcionalidades principales:

- Listado de dispositivos conocidos por el sistema.
- Listado, alta, baja y modificación de ubicaciones.
- Listado, alta, baja y modificación de pacientes.
- Listado, alta, baja y modificación de internaciones.

# Menú "Dispositivos"

![Icono del menú dispositivos](files/device.png)

Este menú presenta un listado de los dispositivos conocidos por el sistema.

![Menú "Dispositivos"](files/devices_window.png)

Para cada dispositivo se lista:

- La dirección MAC del dispositivo.
- La fecha y hora en que el sistema vió por primera vez al dispositivo.
- La fecha y hora del último mensaje visto por el sistema.
- El último valor de tensión de batería.
- El epoch Unix de la última transmisión de datos de batería.
- El último ID de mensaje.

# Menú "Ubicaciones"

![Icono del menú ubicaciones](files/location.png)

Este menú presenta un listado de las ubicaciones cargadas en el sistema.

![Menú "Ubicaciones"](files/locations_window.png)

Para cada ubicación se lista:

- El ID asociado.
- El tipo de ubicación:
    - Local: ubicación dentro de las instalaciones del centro asistencial.
    - External: internaciones que se monitorean por fuera del centro asistencial.
- Descripción: texto libre que describe la ubicación, por ejemplo: "Piso 1 Cama 101".

Se permite al usuario agregar, modificar y borrar ubicaciones.

## Agregado de ubicaciones

Al pulsar el botón "agregar" se abre el siguiente diálogo:

![Agregar ubicación](files/location_add.png)

# Menú "Pacientes"

![Icono del menú pacientes](files/patient.png)

Este menú presenta el listado de pacientes conocidos por el sistema.

![Menú "Pacientes"](files/patients_window.png)

Para cada paciente se lista:

- El ID asociado.
- La fecha de ingreso al sistema.
- Nombre.
- Apellido.
- Edad.
- Sexo: f(emenino), m(asculino) u o(tro).
- DNI: número sin puntos.
- Comentarios: texto libre.

Se permite al usuario agregar, modificar y borrar pacientes.

## Agregado de pacientes

Al pulsar el botón "Agregar..." se abre el siguiente diálogo:

![Agregar paciente](files/patient_add.png)

# Menú "Internaciones"

![Icono del menú internaciones](files/internment.png)

Este menú presenta el listado de internaciones conocidas por el sistema.

![Menú "Internaciones"](files/internments_window.png)

Por cada internación se lista:

- El ID asociado.
- La fecha de admisión.
- La fecha de egreso, vacía en caso de que la internación se mantenga activa.
- Apellido y nombre del paciente asociado a la internación.
- Ubicación asignada.
- Dispositivo asignado.

Se permite al usuario agregar, modificar y borrar internaciones.

## Agregado de internaciones

Al pulsar el botón "Agregar..." se abre el siguiente diálogo:

![Agregar internación](files/add_internment.png)

El diálogo permite elegir paciente, ubicación y dispositivo para generar una internación siempre y cuando no estén ya asociados a una internación activa. Si pueden estar asociados a internaciones finalizadas.