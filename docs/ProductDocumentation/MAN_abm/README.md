- [MoSimPa ABM (Alta, Baja y Modificación)](00.md#mosimpa-abm-alta-baja-y-modificacion).
- [Menú "Dispositivos"](00.md#menu-dispositivos).
- [Menú "Ubicaciones"](00.md#menu-ubicaciones).
- [Menú "Pacientes"](00.md#menu-pacientes).
- [Menú "Internaciones"](00.md#menu-internaciones).
