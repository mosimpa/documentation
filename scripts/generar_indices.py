#!/usr/bin/python3

import os
import re
import unicodedata

def slugify(value, separator, encoding='ascii'):
    """ Slugify a string, to make it URL friendly. """
    value = unicodedata.normalize('NFKD', value).encode(encoding, 'ignore')
    value = re.sub(r'[^\w\s-]', '', value.decode(encoding)).strip().lower()
    return re.sub(r'[{}\s]+'.format(separator), separator, value)


def slugify_unicode(value, separator):
    """ Slugify a string, to make it URL friendly while preserving Unicode characters. """
    return slugify(value, separator, 'utf-8')


root = "../docs"

# docs/README.md
rootIndexPath = os.path.join(root, 'README.md')
if os.path.exists(rootIndexPath):
    os.remove(rootIndexPath)

with open(rootIndexPath, 'a', encoding="utf8") as rootIndex:

    # Check wether README.md.in exists.
    if os.path.exists(os.path.join(root, 'README.md.in')):
        # Copy README.md.in into README.md
        with open(os.path.join(root, 'README.md.in'), encoding="utf8") as skel:
            for line in skel:
                rootIndex.write(line)


    # Get the top level directories under docs
    for actividad in next(os.walk(root))[1]:
        if actividad == "images":
            continue
        print("Procesando actividad {}".format(actividad))

        link = "- [{}]({}/README.md).\n".format(actividad, actividad)
        rootIndex.write(link)

        actividadIndexPath = os.path.join(root, actividad, 'README.md')
        if os.path.exists(actividadIndexPath):
            os.remove(actividadIndexPath)

        with open(actividadIndexPath, 'a', encoding="utf8") as actividadIndexFile:

             #Check wether README.md.in exists.
             #if os.path.exists(os.path.join(root, actividad, 'README.md.in')):
                 #with open(os.path.join(root, actividad, 'README.md.in'), , encoding="utf8") as actividadSkel:
                     #for actividadSkelLineine in actividadSkel:
                         #rootIndex.write(actividadSkelLine)

            for doc in next(os.walk(os.path.join(root, actividad)))[1]:
                print("  Procesando documento {}".format(doc))

                link = "- [{}]({}/README.md).\n".format(doc, doc)
                actividadIndexFile.write(link)

                if os.path.exists(os.path.join(root, actividad, doc, 'README.md')):
                    os.remove(os.path.join(root, actividad, doc, 'README.md'))

                with open(os.path.join(root, actividad, doc, 'README.md'), 'a', encoding="utf8") as index:

                    # Check wether README.md.in exists.
                    if os.path.exists(os.path.join(root, actividad, doc, 'README.md.in')):
                        # Copy README.md.in into README.md
                        with open(os.path.join(root, actividad, doc, 'README.md.in'), encoding="utf8") as skel:
                            for line in skel:
                                index.write(line)

                    # List all the .md files
                    files = os.listdir(os.path.join(root, actividad, doc))
                    # Be sure to sort them.
                    files.sort()

                    for file in files:
                        if re.match('\d\d.*\.md', file) and os.path.isfile(os.path.join(root, actividad, doc, file)):
                            print("    Procesando archivo {}".format(file))
                            with open(os.path.join(root, actividad, doc, file), encoding="utf8") as origin_file:
                                for line in origin_file:
                                    line = re.findall(r'^# .+', line)
                                    if line:
                                        link = "- [{}]({}#{}).\n".format(line[0].replace('# ',''), file, slugify_unicode(line[0], '-'))
                                        index.write(link)
