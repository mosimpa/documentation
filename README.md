# Documentación general del proyecto MoSimPa

El índice general de la documentación se encuentra en docs/README.md o
el generado por mkdocs.yml.

# Generación de los índices

Se requiere de Python3:

```sh
cd scripts
./generar_indices.py
```

# Steps to produce more or less readable PDF:

    sed 's/<p>-/<p>\\-/g' document.md | pandoc -f markdown+raw_html+markdown_in_html_blocks+implicit_header_references+strikeout+tex_math_dollars+raw_tex+yaml_metadata_block -t html -o - | pandoc -f html -N -s --toc -o document.pdf
